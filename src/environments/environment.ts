// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  
  production: false,
  url: 'http://localhost/AngularProject/Slim/',
  firebase: { 
    apiKey: "AIzaSyANKRClwfJnLGucKCXGIGsaOVQfVsP7lMg",
    authDomain: "project-24a2b.firebaseapp.com",
    databaseURL: "https://project-24a2b.firebaseio.com",
    projectId: "project-24a2b",
    storageBucket: "project-24a2b.appspot.com",
    messagingSenderId: "197203199604"
  }
};

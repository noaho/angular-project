import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users/users.service';
import { FirebaseObjectObservable, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';


@Component({
  selector: 'create-message',
  templateUrl: './create-message.component.html',
  styleUrls: ['./create-message.component.css']
})
export class CreateMessageComponent implements OnInit {
  Createform = new FormGroup({
    head: new FormControl('',Validators.required),
    body: new FormControl('',Validators.required)
  });
  messages;
  messagesKeys = [];
  token;
  create$: FirebaseListObservable<any[]>;
  constructor(private service:UsersService , private db:AngularFireDatabase, private formBuilder:FormBuilder ,private router: Router) {
    this.token = localStorage.getItem('token');

   }

   logout(){ 
    this.token = localStorage.removeItem('token');
    console.log(this.token);
}

  ngOnInit() {
    this.create$ = this.db.list('/messages');
    this.Createform = this.formBuilder.group({
      head:  [null, [Validators.required]],
      body: [null, Validators.required],
    });	          
}
  

  createMessage(){
    if(this.token){
    this.create$.push({ body: this.Createform.value.body , head: this.Createform.value.head, likes: 0, user_id: this.token});
    this.router.navigate(['/']);
  }
}

}

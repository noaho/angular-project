import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { UsersService } from '../users.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Angular2TokenService } from 'angular2-token';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
@Component({
  selector: 'create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  @Output() addUser: EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('fileInput') fileInput; //for picture
  //for image
  //fullPath:string;
  //myPicture:string;

  /*getMyPicture(){
    this.fullPath = "../../../public/img/"+this.myPicture;
  }*/

  service:UsersService;
  
    usrform = new FormGroup({
      name:new FormControl(),
      username:new FormControl(),
      password:new FormControl(),
      profile_pic:new FormControl(),
    });
  
    sendData(){
      this.addUser.emit(this.usrform.value.name);
      console.log(this.usrform.value);
      this.service.postUser(this.usrform.value).subscribe(
        response => {
          console.log(response.json());
          this.addUserPs.emit();
          //this.router.navigate(['/']);          
        } );
        this.router.navigate(['/login']);
    };
    constructor(service:UsersService, private formBuilder:FormBuilder, private tokenService:Angular2TokenService,private router: Router) {
      this.service=service;
      this.tokenService.init();
    }

    upload() {
      let fileBrowser = this.fileInput.nativeElement;
      if (fileBrowser.files && fileBrowser.files[0]) {
        const formData = new FormData();
        formData.append("profile picture", fileBrowser.files[0]);
        console.log(fileBrowser.files[0]);
        this.service.upload(formData).subscribe(res => {
          // do stuff w/my uploaded file
        });
      }
    }

  ngOnInit() {
    this.usrform = this.formBuilder.group({
      	      name:  [null, [Validators.required]],
      	      username:[null, [Validators.required]],
              password:[null, [Validators.required]],
             // profile_pic:[null, [Validators.required]]
      	    });	          
  }

}
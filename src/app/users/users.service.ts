import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {environment } from './../../environments/environment';
import {AngularFireDatabase} from 'angularfire2/database';
import 'rxjs/Rx';
@Injectable()
export class UsersService {
  http:Http;
  token;
  
  getUsers(){
  //get users from the slim rest api (Don't say DB)
  return this.http.get(environment.url +'users');
}

putPopular(data, id){
      let options = {
        headers: new Headers({
          'content-type':'application/x-www-form-urlencoded'
        })
      }
      //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
      let params = new HttpParams().append('popular',data);
      return this.http.put(environment.url +'users/'+ id ,params.toString(), options);
    }

    getUser(id){
      /*let options =  {
        headers:new Headers({
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
          'Token': this.token    
        })
      }*/
      return this.http.get(environment.url +'users/'+ id);
    }

postUser(data){
  let options = {
    headers:new Headers({'content-type':'application/x-www-form-urlencoded'})
  }
  let params = new HttpParams().append('name',data.name).append('username',data.username).append('password',data.password).append('profile_pic',data.profile_pic);
  return this.http.post(environment.url +'users',params.toString(),options);
}

//Get image
getImage(id){
  return this.http.get(environment.url +'users/image/'+ id);
}

upload(data) {
  //let headers = this.tokenService.currentAuthHeaders;
  let options = {
    headers:new Headers({'content-type':'application/x-www-form-urlencoded'})
  }
  //headers.delete('Content-Type');
  //let options = new RequestOptions({ headers: headers });
  //let params = new HttpParams().append('profile picture',data.profile_pic);
  return this.http.post(environment.url +'users',data, options);
}

/*upload(formData) {
  //let headers = this.tokenService.currentAuthHeaders;
  //headers.delete('Content-Type');
  //let options = new RequestOptions({ headers: headers });

  return this.tokenService.request({
    method: 'post',
    url: `http://localhost/AngularProject/Slim/users`,
    body: formData,
    headers: options.headers
  }).map(res => res.json());
}*/
login(credentials){
  let options = {
     headers:new Headers({
      'content-type':'application/x-www-form-urlencoded'
     })
  }
 let  params = new HttpParams().append('username', credentials.user).append('password',credentials.password);
 return this.http.post(environment.url +'login', params.toString(),options).map(response=>{ 
   let token = response.json().token;
   if (token){
     localStorage.setItem('token', token);
     console.log(token);
   }
});
}
  

getMessagesFire(){
  return this.db.list('/messages').snapshotChanges().map(changes => {
   return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
} 

getCommentsFire(){
  return this.db.list('/comments').valueChanges();
} 

constructor(http:Http,private db:AngularFireDatabase) {
    this.http = http;
    this.token = localStorage.getItem('token');
   }



}

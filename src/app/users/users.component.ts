import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from './users.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  users;
  usersKeys;
  userKeys;
  messages;
  user;
  route;
  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();
  
  constructor(private service:UsersService) {
    service.getUsers().subscribe(response=>{
        //console.log(response.json());
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
      });
 }

  /*deleteUser(key){    
  let index = this.usersKeys.indexOf(key);
  this.usersKeys.splice(index,1);
  this.service.deleteUser(key).subscribe(
  response=> console.log(response));
  }*/

  ngOnInit() {
   this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
        this.userKeys = Object.keys(this.user);
        //console.log(this.userKeys);
        this.route.paramMap.subscribe(params=>{
          let id = params.get('id');
          console.log(id);
          
          //this.userKeys.forEach(function (item) {
          for (let key of this.userKeys){
            if(key == id){
            console.log(this.user[key].id);
            this.service.putPopular(this.user[key].popular + 1, id).subscribe(
                  response => {
                    console.log(response.json());
                    this.updateUserPs.emit();
                  }
            );
          }
        }
        });
        this.optimisticAdd(this.user, this.userKeys);
      })
    });
     
  

    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.messages = response;
    });
   
  }
  optimisticAdd(user, usersKeys){
      for (let key of usersKeys){
        this.user[key].popular = this.user[key].popular + 1
      }

  }


}
import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from '../users/users.service';
@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  
  token;
  logout(){ 
      this.token = localStorage.removeItem('token');
      console.log(this.token);
  }
  constructor(private service:UsersService) {
    //this.token = localStorage.getItem('token');
   }

  ngOnInit() {
   
  }

}
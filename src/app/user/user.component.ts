import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { UsersService } from '../users/users.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user;
  userKeys = [];
  messages;

  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();
  
    constructor(private route: ActivatedRoute , private service:UsersService) { }
  
    //קריאה של יוזר בודד
    ngOnInit() {
      this.route.paramMap.subscribe(params=>{
        let id = params.get('id');
        console.log(id);
        this.service.getUser(id).subscribe(response=>{
          this.user = response.json();
          console.log(this.user);
          this.userKeys = Object.keys(this.user);
          //console.log(this.userKeys);
          this.route.paramMap.subscribe(params=>{
            let id = params.get('id');
            console.log(id);
            
            //this.userKeys.forEach(function (item) {
            for (let key of this.userKeys){
              if(key == id){
              console.log(this.user[key].id);
              this.service.putPopular(this.user[key].popular + 1, id).subscribe(
                    response => {
                      console.log(response.json());
                      this.updateUserPs.emit();
                    }
              );
            }
          }
          });
          this.optimisticAdd(this.user, this.userKeys);
        })
      });


      this.service.getMessagesFire().subscribe(response=>{
        console.log(response);
        this.messages = response;
      });
    }

    optimisticAdd(user, usersKeys){
      for (let key of usersKeys){
        this.user[key].popular = this.user[key].popular + 1
      }

    }

 
}
import { Component, OnInit } from '@angular/core';
import { FormControl , FormGroup } from '@angular/forms';
import { Router} from "@angular/router";
import {UsersService} from './../users/users.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalid = false;
  loginForm = new FormGroup({
    user:new FormControl(),
    password:new FormControl(),
  });

  login(){
    this.service.login(this.loginForm.value).subscribe(responser=>{
      this.router.navigate(['/']);
    },
    error=>{this.invalid= true;})
  }

  logout(){ 
    localStorage.removeItem('token');
    this.invalid= false;  
  }
  constructor(private service:UsersService, private router:Router) { }
 
  ngOnInit() {
  	var value = localStorage.getItem('token');
  	
  	if (!value || value == undefined || value == "" || value.length == 0){		
  		this.router.navigate(['/login']);  
  	}else{
      this.router.navigate(['/']);  		
  	}
  }

}

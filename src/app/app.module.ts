import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{RouterModule} from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import {EmojiPickerModule} from 'ng-emoji-picker';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessagesComponent } from './messages/messages.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { UsersService } from './users/users.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import { environment } from './../environments/environment';
import { CreateMessageComponent } from './create-message/create-message.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    LoginComponent,
    NotFoundComponent,
    MessagesComponent,
    CreateUserComponent,
    NavigationComponent,
    UpdateUserComponent,
    CreateMessageComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    EmojiPickerModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'',component:MessagesComponent},
      {path:'users', component:UsersComponent},
      {path:"login", component:LoginComponent},
      {path:"messages", component:MessagesComponent},
      {path:"create-user", component:CreateUserComponent},
      {path:"create-message",component:CreateMessageComponent},
      {path:"user/:id",component:UserComponent},
      {path:'**',component:NotFoundComponent}
    ])
  ],
  
  providers: [UsersService,Angular2TokenService, AngularFireDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }

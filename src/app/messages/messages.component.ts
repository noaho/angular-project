import { Component, ViewChild, OnInit , Output , EventEmitter , ElementRef, OnDestroy } from '@angular/core';
import { UsersService } from '../users/users.service';
import { FirebaseObjectObservable, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer , SafeResourceUrl} from '@angular/platform-browser';
import { Observable } from 'rxjs';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  updateform = new FormGroup({
    head: new FormControl('',Validators.required),
    body: new FormControl('',Validators.required),
  });
  commentForm = new FormGroup({
    body: new FormControl('',Validators.required)
  })



  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();
  //showSlim:Boolean = true; //יוזר המאפשר על ידי לחיצה לראות את הנתונים מסלים כאשר הוא שווה לאמת
 token;
  messages;
  messagesKeys = [];
  updates = [];
  lastOpenedToUpdate;//משתנה המשמש לראות איזה עדכון של יוזר מסוים נפתח אחרון
  user;
  userkeys =[];
  create$: FirebaseListObservable<any[]>;
  constructor(private service:UsersService , private db:AngularFireDatabase, private route: ActivatedRoute, private domSanitizer: DomSanitizer) {
    this.token = localStorage.getItem('token');
   }
  _htmlProperty;
  imgs = [];
 img;
  id; 
   user_id;
 messagesPic;
 messageUserId;
 @ViewChild('userId') userId: ElementRef;

 logout(){ 
  this.token = localStorage.removeItem('token');
  console.log(this.token);
}

  //Get Data
  ngOnInit() {
    console.log(this.token);
    if(this.token){
    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.messages = response;
      //this.messagesPic = response;
      this.messagesPic = response.forEach(res =>{
        //this.imgs[res.user_id]  = this.viewImage(res.user_id);
        this.service.getImage(res.user_id).subscribe(i=>{
          this._htmlProperty = i.text();
          this.imgs[res.user_id] = this.domSanitizer.bypassSecurityTrustHtml(this._htmlProperty);
        });
        console.log(this.imgs[res.user_id]);
        return this.imgs[res.user_id] ;
      });
    });
  }
}
  



  showUpdate(id, key){
    console.log(key);
    if(this.updates[id]){//לסגור את העדכון ליוזר
      this.updates[id] = false;
    }else{
      if(this.lastOpenedToUpdate){ //סוגר את העדכון אם למשתמש אחר פתוח
        this.updates[this.lastOpenedToUpdate] = false;
      }
      //מראה את הטופס למשתמש לאחר שלחץ על עדכון
      this.updates[id] = true;
      this.updateform.get('head').setValue(key.head);
      this.updateform.get('body').setValue(key.body);
      this.lastOpenedToUpdate = id;      
    } 
  }
  

  like(id, key){
        //console.log(`${key.$key}`);
        const idMessage = key[`${key}`];
        //let mess = '-L7KMJzgK14cevAohUQ1';
        console.log(idMessage);

    let audio = new Audio();
    audio.src = "assets/applause.mp3";
    audio.load();
    audio.play();
  
    //this.playAudio();

         this.db.object('/messages/'+ id).update({ likes: key.likes + 1});
        
   }

  updateMessage(key){
    console.log(this.updateform.value.body);
    this.db.object('/messages/' + key).update({ body: this.updateform.value.body , head: this.updateform.value.head});   
    this.updates[key] = false;
  }
  addComment(key){
    this.create$.push({ body: this.commentForm.value.body});
  }

  deleteMessage(key){
    this.db.object('/messages/' + key).remove(); 
  }

  
}